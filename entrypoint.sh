#!/bin/bash

export $(cat /credentials | xargs)

if [[ -n "$WEBDAV_URL" ]] && [[ -n "$WEBDAV_USER" ]] && [[ -n "$WEBDAV_PASS" ]]
then
    echo "Starting mount of $WEBDAV_URL with user $WEBDAV_USER"
else
    echo "Not all required environment variables are set"
    exit 1
fi

echo "$WEBDAV_URL/$WEBDAV_USER /mnt/webdav davfs user,noauto,uid=root,file_mode=600,dir_mode=700 0 1" >> /etc/fstab
echo "/mnt/webdav $WEBDAV_USER \"$WEBDAV_PASS\"" >> /etc/davfs2/secrets

mount /mnt/webdav

unison /localsync /mnt/webdav/homesmarthome -auto -batch -prefer newer

echo "Waiting davfs to trigger for file upload, continue in 10s"
sleep 10

# unmount, and wait for transfers
umount.davfs /mnt/webdav

echo "Starting credential cleanup"
echo "" > /etc/davfs2/secrets
export WEBDAV_USER=""
export WEBDAV_PASS=""
echo "Done!"