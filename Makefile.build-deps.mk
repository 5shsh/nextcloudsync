copy_dependencies:
	mkdir -p ./build
	cp ./.circleci/etc/amd64/nothing.sh ./build/pre-build
	cp ./.circleci/etc/amd64/nothing.sh ./build/post-build

cleanup_dependencies:
	rm -f ./pre-build
	rm -f ./post-build