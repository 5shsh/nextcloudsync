#-------------------------------------------------------------------------------
# Base Image Spec
#-------------------------------------------------------------------------------
ARG BASE_IMAGE=debian
ARG BASE_IMAGE_TAG=jessie-slim
ARG BASE_IMAGE_NAMESPACE=

FROM ${BASE_IMAGE_NAMESPACE}${BASE_IMAGE}:${BASE_IMAGE_TAG}

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY ./build/pre-build /usr/bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------

RUN mkdir -p /var/cache/apt/archives/ && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
    davfs2 \
    unison \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /mnt/webdav && mkdir /localsync

COPY entrypoint.sh /entrypoint.sh

RUN chmod +x entrypoint.sh

ENV WEBDAV_URL=http://homesmarthome.5square.de/remote.php/dav/files

ENTRYPOINT /entrypoint.sh

# Stops container after running for $TTL seconds
HEALTHCHECK --retries=1 \
  CMD TTL="30"; if [ "$(($(date +%s) - $(stat -c %Z /proc/)))" -gt "$TTL" ]; then exit 1; fi; exit 0

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY ./build/post-build /usr/bin/cross-build-end
RUN [ "cross-build-end" ]

#-------------------------------------------------------------------------------
# Labelling
#-------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION
LABEL de.5square.homesmarthome.build-date=$BUILD_DATE \
      de.5square.homesmarthome.name="homesmarthome/nextcloudsync" \
      de.5square.homesmarthome.description="Synching config files bi-directional from all homesmarthome instances to cloud" \
      de.5square.homesmarthome.url="homesmarthome.5square.de" \
      de.5square.homesmarthome.vcs-ref=$VCS_REF \
      de.5square.homesmarthome.vcs-url="$VCS_URL" \
      de.5square.homesmarthome.vendor="5square" \
      de.5square.homesmarthome.version=$VERSION \
      de.5square.homesmarthome.schema-version="1.0"
