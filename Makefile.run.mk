docker_run:
	mkdir -p ./build/testrun_config
	docker run -d \
	  --privileged \
	  --rm \
	  --name=nextcloudsync_test_run \
	  -v $(PWD)/test/.nextcloudsync:/credentials \
	  -v $(PWD)/build/testrun_config:/localsync \
	  $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker ps | grep nextcloudsync_test_run

docker_stop:
	docker rm -f nextcloudsync_test_run
	rm -rf ./testrun_config