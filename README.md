# NextCloudSync #

[![](https://images.microbadger.com/badges/image/homesmarthome/nextcloudsync.svg)](https://microbadger.com/images/homesmarthome/nextcloudsync "Get your own image badge on microbadger.com")

[![](https://images.microbadger.com/badges/version/homesmarthome/nextcloudsync.svg)](https://microbadger.com/images/homesmarthome/nextcloudsync "Get your own version badge on microbadger.com")

Local config dir (e.g. ```/config```) is mounted to container. Container mounts itself ```homesmarthome``` folder from personal nextcloud account (http://homesmarthome.5square.de) and syncs bi-directional content from webdav folder to mounted config folder.
Credentials have to be set in an environment file used in the ```run``` command in the following format:
```
WEBDAV_USER=i_am
WEBDAV_PASS=my_password
```

## Shuttle
As this container needs privileged mode to mount davfs2 folder, a Swarm shuttle service is needed for starting a normal container, as Swarm is not able to provide the necessary privileges.

See [bitbucket.org/5shsh/nextcloudsync_shuttle](https://bitbucket.org/5shsh/nextcloudsync_shuttle)


### Run (Deprecated)
```
docker run -d \
  --restart=unless-stopped \
  --name=nextcloudsync \
  -v /home/pi/.nextcloudsync:/credentials \
  -v /config:/localsync \
  --privileged \
  homesmarthome/nextcloudsync:latest
```
